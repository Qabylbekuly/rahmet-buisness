<?php

Route::group(['namespace' => 'API\Admin', 'prefix' => 'admin/', 'middleware' => ['auth:api', 'admin']], function () {
    Route::resource('/categories', 'CategoryController')->only([
        'index', 'show', 'store', 'update', 'destroy'
    ]);
    Route::resource('/products', 'ProductController')->only([
        'index', 'show', 'store', 'update', 'destroy'
    ]);
    Route::resource('/properties', 'PropertyController')->only([
        'index', 'show', 'store', 'update', 'destroy'
    ]);
    Route::resource('/property-values', 'ProductController')->only([
        'index', 'show', 'store', 'update', 'destroy'
    ]);
    Route::resource('/model-property-values', 'ModelPropertyValueController')->only([
        'index', 'show', 'store', 'update', 'destroy'
    ]);
    Route::resource('/users', 'UserController')->only([
        'index', 'show', 'store', 'update', 'destroy'
    ]);
});
