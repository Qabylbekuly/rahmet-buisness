<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'API'], function () {

    Route::group(['prefix' => 'auth'], function () {
        Route::post('login', 'AuthController@login');
        Route::post('register', 'AuthController@registration');
        Route::post('logout', 'AuthController@logout');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('me', 'AuthController@me');
    });
    Route::group(['middleware' => ['auth:api']], function () {
        Route::group(['prefix' => 'profile/'], function () {
            Route::get('me', 'ProfileController@me');
        });
    });

    Route::group(['prefix' => 'categories/'], function () {
        Route::get('', 'CategoryController@index');
        Route::get('show/{id}', 'CategoryController@show');
    });
    Route::group(['prefix' => 'products/'], function () {
        Route::get('', 'ProductController@index');
        Route::get('show/{id}', 'ProductController@show');
    });
});



