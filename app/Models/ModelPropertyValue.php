<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelPropertyValue extends Model
{
    protected $fillable = [
        'id',
        'property_value_id',
        'model_type',
        'model_id',
    ];
}
