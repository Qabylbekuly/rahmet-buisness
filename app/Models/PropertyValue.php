<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyValue extends Model
{
    protected $fillable = [
    'id',
    'property_id',
    'value',
    'title',
    'priority',
    'settings'
];

}
