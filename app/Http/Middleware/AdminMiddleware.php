<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ($user->hasRole(['admin', 'moderator'], true)) {
            return response()->json([
                'message' => 'You don"t have enough permissions',
            ], Response::HTTP_BAD_REQUEST);
        }

        return $next($request);
    }
}
