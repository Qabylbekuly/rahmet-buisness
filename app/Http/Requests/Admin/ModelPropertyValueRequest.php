<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ModelPropertyValueRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];
        $rules['model_property_value.property_value_id'] = 'required|max:255';
        $rules['model_property_value.model_type'] = 'required|max:255';
        $rules['model_property_value.model_id'] = 'required|max:255';
        return $rules;
    }
}
