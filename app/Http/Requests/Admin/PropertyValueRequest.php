<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PropertyValueRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];

        $rules['property_value.id'] = 'required|max:255';
        $rules['property_value.priority'] = 'required|max:255';
        $rules['property_value.property_id'] = 'required|max:255';
        $rules['property_value.settings'] = 'required|max:255';
        $rules['property_value.title'] = 'required|max:255';
        $rules['property_value.value'] = 'required|max:255';
        return $rules;
    }
}
