<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];

        $rules['property.id'] = 'required|max:255';
        $rules['property.name'] = 'required|max:255';
        $rules['property.slug'] = 'required|max:255';
        $rules['property.type'] = 'required|max:255';
        $rules['property.configuration'] = 'required|max:255';
        return $rules;
    }
}
