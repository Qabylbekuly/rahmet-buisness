<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class RegionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [];

        $rules['country_id'] = 'required|integer';
        $rules['region_name'] = 'required|string|max:255';
        $rules['region_code'] = 'nullable|string|max:50';

        return $rules;
    }
}
