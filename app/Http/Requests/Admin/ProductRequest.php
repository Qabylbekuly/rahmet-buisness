<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];

        $rules['id'] = 'required|max:255';
        $rules['name'] = 'required|max:255';
        $rules['slug'] = 'required|max:255';
        $rules['price'] = 'required|max:255';
        $rules['stock_price'] = 'required|max:255';
        $rules['product_number'] = 'required|max:255';
        $rules['excerpt'] = 'required|max:255';
        $rules['description'] = 'required|max:255';
        $rules['status'] = 'required|max:255';
        $rules['partner_id'] = 'required|max:255';
        return $rules;
    }
}
