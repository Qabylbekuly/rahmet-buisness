<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [];

        $rules['name'] = 'required|string';
        $rules['email'] = 'required|string|email|unique:users';
        $rules['password'] = 'required|string|confirmed';
        $rules['role_id'] = 'required|integer';
        return $rules;
    }
}
