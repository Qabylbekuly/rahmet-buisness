<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];

        $rules['name'] = 'required|string';
        $rules['email'] = 'required|string|email|unique:users';
        $rules['password'] = 'required|string|confirmed';
        return $rules;
    }
}
