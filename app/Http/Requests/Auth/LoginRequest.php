<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $rules = [];

        $rules['email'] = 'required|string|exists:users,email';
        $rules['password'] = 'required|string';

        return $rules;
    }
}
