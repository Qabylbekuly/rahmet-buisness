<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AnnouncementRecipient;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProfileController extends Controller
{
    public function me()
    {
        $user = auth()->user();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'user' => $user,
        ], Response::HTTP_OK);
    }
}
