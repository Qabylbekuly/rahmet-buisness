<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\LogRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{

    public function index()
    {
        $categories = Category::with('products')->orderByDesc('created_at')->get();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'categories' => $categories,
        ], Response::HTTP_OK);
    }

    public function show($id)
    {
        $category = Category::with('products')->where('id',$id)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'category' => $category
        ], Response::HTTP_OK);
    }


}
