<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Http\Requests\Admin\PropertyRequest;
use App\Http\Requests\Admin\PropertyValueRequest;
use App\Models\Property;
use App\Models\PropertyValue;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PropertyValueController extends Controller
{

    public function index()
    {
        $propertie_values = PropertyValue::orderBy('id', 'desc')->paginate(12);
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'propertie_values' => $propertie_values,
        ], Response::HTTP_OK);
    }

    public function store(PropertyValueRequest $request)
    {
        $property_value = PropertyValue::create($request->validated());
        return response()->json([
            'result' => 1,
            'message' => 'Property successfully created',
            'property_value' => $property_value,
        ], Response::HTTP_CREATED);
    }

    public function update(ProductRequest $request, $id)
    {
        $property_value = PropertyValue::findOrFail($id);
        $property_value->fill($request->except(['property_value_id']));
        $property_value->save();
        return response()->json([
            'message' => 'Property updated successfully',
            'property_value' => $property_value,
        ], Response::HTTP_OK);
    }

    public function show($property_id)
    {
        $property = Property::where('id', $property_id)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'product' => $property,
        ], Response::HTTP_OK);
    }

    public function destroy($property_id)
    {

        $property = Property::where('id', $property_id)->delete();
        return response()->json([
            'message' => 'Product successfully deleted'
        ], Response::HTTP_OK);
    }
}
