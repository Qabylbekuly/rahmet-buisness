<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Http\Requests\Admin\PropertyRequest;
use App\Models\Product;
use App\Models\Property;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PropertyController extends Controller
{


    public function index()
    {
        $properties = Property::orderBy('id', 'desc')->paginate(12);
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'properties' => $properties,
        ], Response::HTTP_OK);
    }

    public function store(ProductRequest $request)
    {
        $property = Property::create($request->validated());
        return response()->json([
            'result' => 1,
            'message' => 'Property successfully created',
            'property' => $property,
        ], Response::HTTP_CREATED);
    }

    public function update(ProductRequest $request, $id)
    {
        $property = Property::findOrFail($id);
        $property->fill($request->except(['property_id']));
        $property->save();
        return response()->json([
            'message' => 'Property updated successfully',
            'property' => $property,
        ], Response::HTTP_OK);
    }


    public function show($property_id)
    {
        $property = Property::where('id', $property_id)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'product' => $property,
        ], Response::HTTP_OK);
    }

    public function destroy($property_id)
    {

        $property = Property::where('id', $property_id)->delete();
        return response()->json([
            'message' => 'Product successfully deleted'
        ], Response::HTTP_OK);
    }
}
