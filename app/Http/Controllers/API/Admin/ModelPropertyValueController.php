<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Http\Requests\Admin\ModelPropertyValueRequest;
use App\Models\Category;
use App\Models\ModelPropertyValue;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ModelPropertyValueController extends Controller
{
    public function index()
    {
        $model_property_values = ModelPropertyValue::orderByDesc('created_at')->get();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'model_property_values' => $model_property_values,
        ], Response::HTTP_OK);
    }


    public function store(ModelPropertyValueRequest $request)
    {
        $model_property_value = ModelPropertyValue::create($request->validated());
        return response()->json([
            'message' => 'Model Property Value created successfully',
            'model_property_value' => $model_property_value,
        ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $model_property_value = ModelPropertyValue::where('id', $id)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'model_property_value' => $model_property_value
        ], Response::HTTP_OK);
    }

    public function update(ModelPropertyValueRequest $request, $id)
    {
        $model_property_value = ModelPropertyValue::findOrFail($id);
        $model_property_value->fill($request->except(['model_property_value_id']));
        $model_property_value->save();
        return response()->json([
            'message' => 'Model Property Value updated successfully',
            'model_property_value' => $model_property_value,
        ], Response::HTTP_OK);
    }

    public function destroy($id)
    {
        try {
            $model_property_value = ModelPropertyValue::findOrFail($id);
            $model_property_value->delete();
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Error deleting category',
                'error' => $exception->getMessage()
            ], $exception->getCode());
        }
        return response()->json([
            'message' => 'Model property value deleted successfully'
        ], Response::HTTP_OK);
    }
}
