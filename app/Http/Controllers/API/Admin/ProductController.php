<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Models\Product;
use App\Http\Requests\Admin\CategoryRequest;
use App\Models\Category;
use App\Models\Country;
use App\Repositories\CategoryRepository;
use App\Repositories\LogRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    public function index()
    {
        $products = Product::with('categories')->orderByDesc('created_at')->get();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'products' => $products,
        ], Response::HTTP_OK);
    }


    public function store(ProductRequest $request)
    {
        $product = Product::create($request->validated());
        return response()->json([
            'message' => 'Product created successfully',
            'product' => $product,
        ], Response::HTTP_CREATED);
    }

    public function update(ProductRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->fill($request->except(['product_id']));
        $product->save();
        return response()->json([
            'message' => 'Product updated successfully',
            'product' => $product,
        ], Response::HTTP_OK);
    }

    public function show($id)
    {
        $product = Product::with('categories')->where('id', $id)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'product' => $product
        ], Response::HTTP_OK);
    }


    public function destroy($id)
    {
        try {
            $product = Product::findOrFail($id);
            $product->delete();
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Error deleting category',
                'error' => $exception->getMessage()
            ], $exception->getCode());
        }

        return response()->json([
            'message' => 'Category deleted successfully'
        ], Response::HTTP_OK);
    }
}
