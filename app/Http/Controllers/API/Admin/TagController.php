<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Http\Requests\Admin\TagRequest;
use App\Models\Product;
use App\Models\ProductTag;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TagController extends Controller
{

    public function index()
    {
        $tags = ProductTag::orderByDesc('created_at')->get();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'tags' => $tags,
        ], Response::HTTP_OK);
    }


    public function store(TagRequest $request)
    {
        $tag = ProductTag::create($request->validated());
        return response()->json([
            'message' => 'Tag created successfully',
            'tag' => $tag,
        ], Response::HTTP_CREATED);
    }

    public function update(ProductRequest $request, $id)
    {
        $tag = ProductTag::findOrFail($id);
        $tag->fill($request->except(['product_id']));
        $tag->save();
        return response()->json([
            'message' => 'Tag updated successfully',
            'tag' => $tag,
        ], Response::HTTP_OK);
    }

    public function show($id)
    {
        $tag = ProductTag::with('categories')->where('id', $id)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'tag' => $tag
        ], Response::HTTP_OK);
    }


    public function destroy($id)
    {
        try {
            $tag = ProductTag::findOrFail($id);
            $tag->delete();
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Error deleting category',
                'error' => $exception->getMessage()
            ], $exception->getCode());
        }

        return response()->json([
            'message' => 'Tag deleted successfully'
        ], Response::HTTP_OK);
    }
}
