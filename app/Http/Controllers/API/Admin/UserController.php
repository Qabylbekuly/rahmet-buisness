<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProductRequest;
use App\Http\Requests\Admin\UserRequest;
use App\Models\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderByDesc('created_at')->get();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'users' => $users,
        ], Response::HTTP_OK);
    }


    public function store(UserRequest $request)
    {
        $user = User::create($request->validated());
        $role = config('roles.models.role')::where('id', '=', $request->role_id)->first();  //choose the default role upon user creation.
        $user->attachRole($role);
        $roles = DB::table('role_user')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->select('role_user.*', 'roles.*'
            )->where('role_user.user_id', $user->id)
            ->first();
        return response()->json([
            'message' => 'User created successfully',
            'user' => $user,
            'roles' => $roles,
        ], Response::HTTP_CREATED);
    }

    public function update(UserRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->fill($request->except(['user_id']));
        $user->save();
        return response()->json([
            'message' => 'User updated successfully',
            'user' => $user,
        ], Response::HTTP_OK);
    }

    public function show($user)
    {
        $user = User::where('id', $user)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'user' => $user
        ], Response::HTTP_OK);
    }


    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user->delete();
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Error deleting category',
                'error' => $exception->getMessage()
            ], $exception->getCode());
        }

        return response()->json([
            'message' => 'User deleted successfully'
        ], Response::HTTP_OK);
    }
}
