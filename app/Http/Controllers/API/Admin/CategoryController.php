<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CategoryRequest;
use App\Models\Category;
use App\Models\Country;
use App\Repositories\CategoryRepository;
use App\Repositories\LogRepository;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{


    public function index()
    {
        $categories = Category::with('products')->orderByDesc('created_at')->get();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'categories' => $categories,
        ], Response::HTTP_OK);
    }


    public function store(CategoryRequest $request)
    {
        $category = Category::create($request->validated());
        return response()->json([
            'message' => 'Category created successfully',
            'category' => $category,
        ], Response::HTTP_CREATED);
    }

    public function show($id)
    {
        $category = Category::with('products')->where('id', $id)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'category' => $category
        ], Response::HTTP_OK);
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = Category::findOrFail($id);
        $category->fill($request->except(['category_id']));
        $category->save();
        return response()->json([
            'message' => 'Category updated successfully',
            'category' => $category,
        ], Response::HTTP_OK);
    }

    public function destroy($id)
    {
        try {
            $category = Category::findOrFail($id);
            $category->delete();
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Error deleting category',
                'error' => $exception->getMessage()
            ], $exception->getCode());
        }

        return response()->json([
            'message' => 'Category deleted successfully'
        ], Response::HTTP_OK);
    }
}
