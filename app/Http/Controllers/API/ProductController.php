<?php

namespace App\Http\Controllers\API;

use App\City;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Tours;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $filter = json_decode($request->filter, true);
        if (
            isset($filter['sort']) && strlen($filter['sort']) >= 1 ||
            isset($filter['tags']) && strlen($filter['tags']) >= 1 ||
            isset($filter['properties']) && strlen($filter['properties']) >= 1 ||
            isset($filter['category']) && strlen($filter['category']) >= 1) {
            $products_ids = [];
            if (isset($filter['category']) && strlen($filter['category']) >= 1) {
                $products_ids = DB::table('category_product')->where('category_id', $filter['category'])->pluck('id');
            }
            if (isset($filter['properties']) && strlen($filter['properties']) >= 1) {
                $properties = explode(',', $filter['properties']);
                if (isset($products_ids)&&count($products_ids) >= 1) {
                    $products_ids = DB::table('model_property_values')
                        ->whereIn('property_value_id', $properties)
                        ->whereIn('model_id', $products_ids)
                        ->pluck('model_id');
                } else {
                    $products_ids = DB::table('model_property_values')
                        ->whereIn('property_value_id', $properties)
                        ->pluck('model_id');
                }

            }
            if (isset($filter['tags']) && strlen($filter['tags']) >= 1) {
                $tags = explode(',', $filter['tags']);
                if (isset($products_ids)&&count($products_ids) >= 1) {
                    $products = Product::whereIn('id', $products_ids)
                        ->where(function ($query) use ($tags) {
                            for ($i = 0; $i < count($tags); $i++) {
                                $query->orwhere('tags', 'LIKE', '%' . '"' . $tags[$i] . '"' . '%');
                            }
                        });
                } else {
                    $products = Product::where(function ($query) use ($tags) {
                        for ($i = 0; $i < count($tags); $i++) {
                            $query->orwhere('tags', 'LIKE', '%' . '"' . $tags[$i] . '"' . '%');
                        }
                    });
                }
            } else {
                $products = Product::whereIn('id', $products_ids);
            }

        } else {
            $products = Product::with('categories');
        }
        $products = $products->orderBy('price',$filter['sort'])->get();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'products' => $products,
        ], Response::HTTP_OK);
    }

    public function show($id)
    {
        $category = Category::with('products')->where('id', $id)->first();
        return response()->json([
            'message' => 'Successfully get to the endpoint',
            'category' => $category
        ], Response::HTTP_OK);
    }
}
