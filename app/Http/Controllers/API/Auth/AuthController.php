<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\User\PhoneVerifyRequest;
use App\Http\Requests\User\ValidatePasswordRequest;
use App\Repositories\LogRepository;
use App\Repositories\UserRepository;
use App\Repositories\VerificationCodeRepository;
//use App\Services\Auth\SmsService;
use App\Services\Auth\SmsService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthController extends Controller
{
    protected $repository;
    protected $codeRepository;
    protected $logRepository;
//    protected $sms;

    public function __construct(UserRepository $repository, VerificationCodeRepository $codeRepository, LogRepository $logRepository)
    {
        $this->repository = $repository;
        $this->codeRepository = $codeRepository;
        $this->logRepository = $logRepository;
    }

    public function login(LoginRequest $request)
    {
        try {
            $user = auth()->attempt($request->all());

            if (!$user) {
                return response()->json([
                    'errors' => array(array('Введенные Вами данные неверены'))
                ], 422);
            }

            $token = auth()->user()->createToken(config('app.name'))->accessToken;

            $user = auth()->user()->load(['roles', 'wallet', 'package', 'status']);

        } catch (\Exception $e) {
            return response()->json([
                'message' => 'Ошибка при авторизации',
                'error' => $e->getMessage()
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->logRepository->store([
            'user_id' => $user->id,
            'action_type' => 'user.auth.login'
        ]);

        return response()->json([
            'message' => 'Вы успешно вошли',
            'user' => $user,
            'role' => $user->roles[0]->name,
            'token_type' => 'Bearar',
            'token' => $token
        ], Response::HTTP_OK);
    }

    public function sendCode(PhoneVerifyRequest $request)
    {
        $user = $this->repository->findByPhone($request->phone);

        if ($user === null) {
            return response()->json([
                'message' => 'Something went wrong'
            ], Response::HTTP_BAD_REQUEST);
        }
        try {
            $code = random_int(1000, 9999);
            $token = $this->codeRepository->store([
                'user_id' => $user->id,
                'user_phone' => $user->phone,
                'verification_code' => $code,
                'sent_time' => Carbon::now(),
                'expires_at' => Carbon::now()->addHours(4)
            ]);

//            $this->sms->send($user->phone, $code);

            return response()->json([
                'message' => 'Код отправлен на Ваш номер. Срок данного кода истекает через 4 часа.',
                'code' => $code,
                'sent_time' => $token->sent_time
            ], Response::HTTP_OK);
        } catch (\Exception $exception) {
            return response()->json([
                'message' => 'Произошла ошибка при отправке смс. Попробуйте позднее!',
                'error' => $exception->getMessage()
            ], $exception->getCode());
        }
    }

    public function verifyCode(PhoneVerifyRequest $request)
    {
        $token = $this->codeRepository->findByCodeAndPhone($request->code, $request->phone);

        if ($token === null) {
            return response()->json([
                'message' => 'Код или номер телефона неверно введен',
            ], Response::HTTP_NOT_FOUND);
        }

        if ($token->expires_at < Carbon::now()) {
            return response()->json([
                'message' => 'Код активации истек, попробуйте заново!'
            ], Response::HTTP_BAD_REQUEST);
        }

        $token = $this->codeRepository->update($token, [
            'verification_time' => Carbon::now()
        ]);

        $user = $this->repository->findByPhone($request->phone);

        $this->repository->update($user, [
            'phone_verified_at' => $token->verification_time
        ]);

        return response()->json([
            'message' => 'Номер телефона успешно активирован. Введите Ваш пароль',
            'phone' => $request->phone,
            'token' => $token
        ], Response::HTTP_OK);
    }

    public function update(ValidatePasswordRequest $request)
    {
        $user = $this->repository->findByPhone($request->phone);

        if ($user->phone_verified_at === null) {
            return response()->json([
                'message' => 'Номер телефона еще активирован.'
            ], Response::HTTP_BAD_REQUEST);
        }

        $this->repository->update($user, [
            'password' => bcrypt($request->password)
        ]);

        return response()->json([
            'message' => 'Вы успешно прошли авторизацию!',
        ], Response::HTTP_OK);
    }
}
