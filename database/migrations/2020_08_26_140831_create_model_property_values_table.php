<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModelPropertyValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('model_property_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_value_id')->unsigned();
            $table->morphs('model');
            $table->timestamps();

            $table->foreign('property_value_id')
                ->references('id')
                ->on('property_values')
                ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('model_property_values');
    }
}
