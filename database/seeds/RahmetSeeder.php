<?php

use App\Models\Category;
use App\Models\ModelPropertyValue;
use App\Models\Product;
use App\Models\ProductTag;
use App\Models\Property;
use App\Models\PropertyValue;
use Illuminate\Database\Seeder;

class RahmetSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = ['Соки', 'Вино', 'Виски', 'Вода', 'Водка'];

        foreach ($categories as $category) {
            Category::create([
                'name' => $category,
            ]);
        }
        $product = Product::create([
            'id' => '1',
            'name' => 'Piko 1л',
            'price' => 600,
            'tags' => '["1","2","3"]'
        ]);
        $categories = Category::find([1]);
        $product->categories()->attach($categories);

        $product = Product::create([
            'id' => '2',
            'name' => 'Jack Daniels 1л',
            'price' => 6000,
            'tags' => '["3"]'
        ]);
        $categories = Category::find([3]);
        $product->categories()->attach($categories);

        $product = Product::create([
            'id' => '3',
            'name' => 'Asu 1л',
            'price' => 200,
            'tags' => '[]'
        ]);
        $categories = Category::find([2, 5]);
        $product->categories()->attach($categories);

        $property = Property::create([
            'id' => '1',
            'name' => 'Тип',
            'type' => 'type',
            'slug' => 'type',
            'configuration' => '[]',
        ]);
        $property_value = PropertyValue::create([
            'id' => '1',
            'property_id' => '1',
            'value' => 'алкогольный',
            'title' => 'алокогольный',
            'priority' => '10',
            'settings' => '[]',
        ]);
        $property_value = PropertyValue::create([
            'id' => '2',
            'property_id' => '1',
            'value' => 'Без алкогольный',
            'title' => 'без алокогольный',
            'priority' => '20',
            'settings' => '[]',
        ]);
        $property = Property::create([
            'id' => '2',
            'name' => 'Вкус',
            'type' => 'taste',
            'slug' => 'taste',
            'configuration' => '[]',
        ]);
        $property_value = PropertyValue::create([
            'id' => '3',
            'property_id' => '2',
            'value' => 'Лимонный',
            'title' => 'лимонный',
            'priority' => '10',
            'settings' => '[]',
        ]);
        $property_value = PropertyValue::create([
            'id' => '4',
            'property_id' => '2',
            'value' => 'Мятный',
            'title' => 'мятный',
            'priority' => '20',
            'settings' => '[]',
        ]);

        $model_property_values = ModelPropertyValue::create([
            'property_value_id' => '1',
            'model_type' => 'product',
            'model_id' => '2',
        ]);
        $model_property_values = ModelPropertyValue::create([
            'property_value_id' => '2',
            'model_type' => 'product',
            'model_id' => '1',
        ]);
        $model_property_values = ModelPropertyValue::create([
            'property_value_id' => '2',
            'model_type' => 'product',
            'model_id' => '3',
        ]);


        $model_property_values = ModelPropertyValue::create([
            'property_value_id' => '3',
            'model_type' => 'product',
            'model_id' => '2',
        ]);
        $model_property_values = ModelPropertyValue::create([
            'property_value_id' => '3',
            'model_type' => 'product',
            'model_id' => '1',
        ]);
        $model_property_values = ModelPropertyValue::create([
            'property_value_id' => '2',
            'model_type' => 'product',
            'model_id' => '3',
        ]);


        $product_tag = ProductTag::create([
            'id' => '1',
            'tag_name' => 'дорогие',
        ]);

        $product_tag = ProductTag::create([
            'id' => '2',
            'tag_name' => 'для подарка',
        ]);

        $product_tag = ProductTag::create([
            'id' => '3',
            'tag_name' => 'освежающие',
        ]);

    }
}
